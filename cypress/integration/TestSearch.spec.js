import {
    typeIntoSearchField,
    pressOnSearchButton,
    resultOfSearch
} from '../classes/Header'

describe ('When searching for an item',() => {
    it('should return something on page', () => {
        cy.visit('http://automationpractice.com/index.php')
        typeIntoSearchField('t-shirt'),
        pressOnSearchButton(),
        resultOfSearch()
    })
})

