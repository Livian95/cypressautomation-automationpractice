import {loginAsUser, logoutFromAccount} from '../classes/Login'
import Header from '../classes/Header'
import SendMessage from '../classes/SendMessage'
import TextAssertions from '../classes/Assertions/TextAssertions'



describe('Navigate through site flows', () => {
    beforeEach(() => {
        cy.visit('http://automationpractice.com/index.php')
    })
    it('Login , complete a contact form and sign out', () => {
        Header.clickSignInButton()
        loginAsUser('candrici@pentalog.com','testtest')
        Header.clickContactUsButton()
        SendMessage.typeMessage("Lorem ipsum trololol")
        SendMessage.clickSubmitMessageButton()
        TextAssertions.assertSubjectMandatory()
        SendMessage.selectSubjectHeading('Webmaster')
        SendMessage.clickSubmitMessageButton()
        TextAssertions.assertMessageSent()
        Header.clickHomeButton()
        logoutFromAccount()
    })


})