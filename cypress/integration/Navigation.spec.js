import HeaderSelectors from '../Selectors/HeaderSelectors'
import Header from '../classes/Header'
import {loginAsUser} from '../classes/Login'
import CheckoutButtons from '../classes/CheckoutButtons'
import TextAssertions from '../classes/Assertions/TextAssertions'
import Payments from '../classes/Payments'
import ShoppingCart from '../classes/ShoppingCart'

describe('Navigate through site flows', () => {
    beforeEach(() => {
        cy.visit('http://automationpractice.com/index.php')
    })
    
    it('Navigate main menus,login and shop an item', () => {
    Header.clickOnMainPageMenus('Women', 0)
    cy.get(HeaderSelectors.womenTitleBox).should('contain','Women')
    Header.clickOnMainPageMenus('Dresses',1)
    cy.get(HeaderSelectors.dressesTitleBox).should('contain','Dresses')
    Header.clickOnMainPageMenus('T-shirts',1)
    cy.get(HeaderSelectors.tShirtsTitleBox).should('contain','T-shirts')   
    Header.hooverMainPageMenus('Women',0)
    Header.hooverMainPageMenus('Dresses',1)
    ShoppingCart.clickShoppingCart()
    TextAssertions.validateEmptyCart()
    Header.clickHomeButton()
    Header.typeIntoSearchField('blouse')
    Header.pressOnSearchButton()
    cy.get('[class="product-container"]').trigger('mouseover')
    ShoppingCart.addToCart()
    TextAssertions.validateAddedItem()
    cy.get('[title="Proceed to checkout"]').click() 
    TextAssertions.validateSummaryAfterCheckout()
    cy.get('[class="button btn btn-default standard-checkout button-medium"]').click() 
    loginAsUser('candrici@pentalog.com','testtest') 
    CheckoutButtons.clickCheckoutButton()
    CheckoutButtons.clickCheckoutButton()
    TextAssertions.agreementTooltipAssertion()
    CheckoutButtons.clickCheckbox()
    CheckoutButtons.clickCheckoutButton()
    Payments.clickPayByBankWire()
    Payments.clickConfirmPaymentButton()
    TextAssertions.validateOrderComplete()
    Header.clickHomeButton()
})
})