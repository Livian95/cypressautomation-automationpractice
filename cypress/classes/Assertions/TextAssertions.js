const agreementTooltipAssertion = () => {
    cy.get('[class="fancybox-inner"]').should('contain','You must agree to the terms of service before continuing.')
    cy.get('[title="Close"]').click()
}

const validateOrderComplete = () => {
    cy.get('[class="box"]').should('contain','Your order on My Store is complete.')
}

const validateEmptyCart = () => {
    cy.get('[class="alert alert-warning"]').should('contain', 'Your shopping cart is empty.')
}

const validateAddedItem = () => {
    cy.get('[class="layer_cart_product col-xs-12 col-md-6"]').should('contain','Product successfully added to your shopping cart')
}

const validateSummaryAfterCheckout = () => {
    cy.get('[id="summary_products_quantity"]').should('contain','1 Product')
}
const assertSubjectMandatory = () => {
    cy.get('[class="alert alert-danger"]').should('contain','Please select a subject from the list provided. ')
}
const assertMessageSent = () => {
    cy.get('[class="alert alert-success"]').should('contain','Your message has been successfully sent to our team.')
}

export default {
    agreementTooltipAssertion,
    validateOrderComplete,
    validateEmptyCart,
    validateAddedItem,
    validateSummaryAfterCheckout,
    assertSubjectMandatory,
    assertMessageSent,
}