const clickPayByBankWire = () => {
    cy.get('[title="Pay by bank wire"]').click()
}

const clickConfirmPaymentButton = () => {
    cy.get('[class="button btn btn-default button-medium"]').click()
}

export default{
    clickPayByBankWire,
    clickConfirmPaymentButton,
}