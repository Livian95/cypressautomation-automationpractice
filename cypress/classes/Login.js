const loginAsUser = (email, passwd) => {
    cy.get('[id="email"]').type(`${email}`)
    cy.get('[id="passwd"]').type(`${passwd}`)
    cy.get('[id="SubmitLogin"]').click()
}

const logoutFromAccount = () => {
    cy.get('[title="Log me out"]').click()
}

export default{
    loginAsUser,
    logoutFromAccount,
}