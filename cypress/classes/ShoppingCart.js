const clickShoppingCart = () => {
    cy.get('[title="View my shopping cart"]').click()
}

const addToCart = () => {
    cy.get('[title="Add to cart"]').click()
}

export default {
    clickShoppingCart,
    addToCart,
}