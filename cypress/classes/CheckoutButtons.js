const clickCheckoutButton = () => {
    cy.get('[type="submit"]').contains('Proceed to checkout').click()
}
const clickCheckbox = () => {
    cy.get('[type="checkbox"]').click()
}

export default {
    clickCheckoutButton,
    clickCheckbox,
}