const typeIntoSearchField = (whatToType) => {
    cy.get('[id=search_query_top]').type(`${whatToType}`)
}

const pressOnSearchButton = () => {
    cy.get('[name=submit_search]').click()
}

var resultOfSearch  = () => {
    cy.get('.product_list').should('be.visible')
}
const clickHomeButton = () => {
    cy.get('[title="Return to Home"]').click()
}

const hooverMainPageMenus = (title, position) => {
    cy.get(`[title=${title}]`).eq(position).trigger('mouseover').focus()
}
const clickOnMainPageMenus = (button, eq) => {
    cy.get(`[title=${button}]`).eq(eq).click()
}
const clickSignInButton = () => {
    cy.get('[title="Log in to your customer account"]').click()
}
const clickContactUsButton = () => {
    cy.get('[id="contact-link"]').click()
}


export default {
    typeIntoSearchField,
    pressOnSearchButton,
    resultOfSearch,
    clickHomeButton,
    hooverMainPageMenus,
    clickOnMainPageMenus,
    clickSignInButton,
    clickContactUsButton,
    
}

