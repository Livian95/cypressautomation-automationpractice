const typeMessage = textToType => {
    cy.get('[id="message"]').click().type(`${textToType}`)
    }
const clickSubmitMessageButton = () => {
    cy.get('[id="submitMessage"]').click()
}
const selectSubjectHeading = subject => {
    cy.get(`[id="id_contact"]`).select(`${subject}`)
}

export default {
    typeMessage,
    clickSubmitMessageButton,
    selectSubjectHeading,
}